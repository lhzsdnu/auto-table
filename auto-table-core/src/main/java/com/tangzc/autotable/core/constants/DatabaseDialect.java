package com.tangzc.autotable.core.constants;

public interface DatabaseDialect {

    String MySQL = "MySQL";
    String PostgreSQL = "PostgreSQL";
    String SQLite = "SQLite";
    String Oracle = "Oracle";
    String SQLServer = "SQLServer";
    String DB2 = "DB2";
    String H2 = "H2";
}
